<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role' => $this->string(),
            'status' => $this->string(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            ], $tableOptions);

        $this->createTable('{{%id_status}}', [
            'id' => $this->primaryKey(),
            'kod_status' => $this->string()->notNull()->unique(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);


        $this->createTable('{{jenis_kenderaan}}', [
            'id' => $this->primaryKey(),
            'jenis_kenderaan' => $this->string()->notNull()->unique(),
            'enjin' => $this->string(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);


        $this->createTable('{{%jenis_pegangan}}', [
            'id' => $this->primaryKey(),
            'jenis_pegangan' => $this->string()->notNull()->unique(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);

        $this->createTable('{{%jenis_penugasan}}', [
            'id' => $this->primaryKey(),
            'jenis_penugasan' => $this->string()->notNull()->unique(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);


        $this->createTable('{{%jenis_rosak}}', [
            'id' => $this->primaryKey(),
            'jenis_rosak' => $this->string()->notNull()->unique(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);



        $this->createTable('{{%maklumat_kenderaan}}', [
            'id' => $this->primaryKey(),
            'jenis_kenderaan' => $this->integer(),
            'no_kenderaan' => $this->string(),
            'perjawatan' => $this->integer(),
            'kod_status' => $this->integer(),
            'km' => $this->double(),
            'timestamp' => $this->dateTime(),
            'penugasan' => $this->integer(),
            'pegangan' => $this->integer(),
            'availability' => $this->integer(),
            'catatan' => $this->string(),
            'kenderaan_rosak' => $this->string(),
            'status' => $this->string(),
            'deleted' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);
        $this->insertData();
        $this->addForeignKey('fk_mk_jenis_kenderaan', '{{%maklumat_kenderaan}}', 'jenis_kenderaan', '{{%jenis_kenderaan}}', 'id');
        $this->addForeignKey('fk_mk_perjawatan', '{{%maklumat_kenderaan}}', 'perjawatan', '{{%jenis_kenderaan}}', 'id');
        $this->addForeignKey('fk_mk_penugasan', '{{%maklumat_kenderaan}}', 'penugasan', '{{%jenis_penugasan}}', 'id');
        $this->addForeignKey('fk_mk_pegangan', '{{%maklumat_kenderaan}}', 'pegangan', '{{%jenis_pegangan}}', 'id');
    }

    public function insertData() {

        $this->insert('{{%user}}', [
            'username' => 'admin1',
            'email' => 'admin1@mail.com',
            'role' => 'Administrator',
            'status' => 1,
            'password_hash' => \Yii::$app->security->generatePasswordHash('admin1'),
            'auth_key' => \Yii::$app->security->generateRandomString(),
            ]);

        $this->insert('{{%jenis_pegangan}}', [
            'jenis_pegangan' => 'Pegangan KOMP ANG AM',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);

        $this->insert('{{%jenis_pegangan}}', [
            'jenis_pegangan' => 'Pegangan HQ/BEKALAN',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);

        $this->insert('{{%jenis_pegangan}}', [
            'jenis_pegangan' => 'Pegangan KOMPOSIT',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);

        $this->insert('{{jenis_kenderaan}}', [
            'jenis_kenderaan' => 'HANDALAN 1',
            'enjin' => 'MITSUBISHI',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);

        $this->insert('{{jenis_kenderaan}}', [
            'jenis_kenderaan' => 'HANDALAN 2',
            'enjin' => 'MITSUBISHI',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);

        $this->insert('{{jenis_kenderaan}}', [
            'jenis_kenderaan' => 'Motosikal Kriss',
            'enjin' => 'Kriss',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_penugasan}}', [
            'jenis_penugasan' => 'Latihan',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_penugasan}}', [
            'jenis_penugasan' => 'Peluru',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_penugasan}}', [
            'jenis_penugasan' => 'Operasi',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_penugasan}}', [
            'jenis_penugasan' => 'Tadbir',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_rosak}}', [
            'jenis_rosak' => 'Dalam Platun',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
        $this->insert('{{%jenis_rosak}}', [
            'jenis_rosak' => 'Dalam 21 DBK',
            'status' => '0',
            'deleted' => 0,
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            ]);
                $this->insert('{{%id_status}}', [
            'kod_status' => 'B',
            'status' => 'Kerosakan memerlukan pembaikan di pasukan',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'C',
            'status' => 'Kerosakan memerlukan pembaikan di Workshop JLJ',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'W',
            'status' => 'Kenderaan/Peralatan dalam Workshop JLJ',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'M',
            'status' => 'Membuat rawatan dan senggaraan servis',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'X',
            'status' => 'Pemeriksaan Teknikal atau Bulanan BAT M 60/61',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => '(M)',
            'status' => 'Rawatan/Senggaraan telah disiapkan',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => '(X)',
            'status' => 'Pemeriksaan telah disiapkan',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'O',
            'status' => 'Kenderaan/Peralatan boleh digerakan',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'TBG',
            'status' => 'Kenderaan belum diselenggara',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);
        $this->insert('{{%id_status}}', [
            'kod_status' => 'TBW',
            'status' => 'Kenderaan menghampiri waktu selenggara',
            'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP()'),
        ]);

    }

    public function down() {
        $this->dropForeignKey('fk_mk_jenis_kenderaan', '{{%maklumat_kenderaan}}');
        $this->dropForeignKey('fk_mk_perjawatan', '{{%maklumat_kenderaan}}');
        $this->dropForeignKey('fk_mk_penugasan', '{{%maklumat_kenderaan}}');
        $this->dropForeignKey('fk_mk_pegangan', '{{%maklumat_kenderaan}}');

        $this->dropTable('{{%user}}');
        $this->dropTable('{{%id_status}}');
        $this->dropTable('{{%jenis_kenderaan}}');
        $this->dropTable('{{%jenis_penugasan}}');
        $this->dropTable('{{%jenis_pegangan}}');
        $this->dropTable('{{%jenis_rosak}}');
        $this->dropTable('{{%maklumat_kenderaan}}');
    }

}
