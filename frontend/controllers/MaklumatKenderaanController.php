<?php

namespace frontend\controllers;

use Yii;
use common\models\MaklumatKenderaan;
use common\models\MaklumatKenderaanSearch;
use common\models\IdStatus;
use common\models\IdStatusSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\components\AccessRule;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
//use kartik\grid\EditableColumnAction;
//use yii\helpers\ArrayHelper;
//use app\models\IdStatus;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * MaklumatKenderaanController implements the CRUD actions for MaklumatKenderaan model.
 */
class MaklumatKenderaanController extends Controller {

    public $enableCsrfValidation = false;

    public function behaviors() {
        return [
            'verbs' =>
            [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'timestamp',
                'updatedAtAttribute' => 'timestamp',
                'value' => date('Y-m-d H:i:s'),
            ],
            'access' =>
            [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
//                        'roles' => [User::ROLE_ADMIN, User::ROLE_USER, User::ROLE_KJ],
                    //                        'roles' => [User::ROLE_AJUTAN],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    //                        'roles' => [User::ROLE_KJ,User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all MaklumatKenderaan models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MaklumatKenderaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new IdStatusSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');
            $model = MaklumatKenderaan::findOne($id);

            $out = Json::encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['MaklumatKenderaan']);
            $post = ['MaklumatKenderaan' => $posted];
            if ($model->load($post)) {
                if ($model->km >= '2000') {
                    $model->kod_status = 9;
                } else if ($model->km >= 1500) {
                    $model->kod_status = 10;
                } else if ($model->km <= 1500) {
                    $model->kod_status = 8;
                } else {

                }
                if ($model->kod_status === 9 || $model->kod_status === 1 || $model->kod_status === 2) {
                    $model->availability = 0;
                    $model->status = 'Approve';
                }
                $model->timestamp = date('Y-m-d H:i:s');
                $model->save(false);

//                $output->$km;
                $output = '';
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
//            if (isset($posted['km'])) {
//
//                $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//            }
            echo $out;
            return;
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'searchModel2' => $searchModel2,
                    'dataProvider2' => $dataProvider2,
        ]);
    }

    public function actionSummary() {

        $unassignvehicle = MaklumatKenderaan::find()
                ->where(['penugasan' => ''])
                ->orWhere(['penugasan' => null])
//                ->asArray()->all();
                ->count();
        $assignvehicle = MaklumatKenderaan::find()
                ->where(['penugasan' => !''])
                ->orWhere(['penugasan' => !null])
//                ->asArray()->all();
                ->count();
//        echo'<pre>';
//        var_dump($assignvehicle);
//        var_dump($unassignvehicle);
//        die();

        return $this->render('summary');
    }

    public function actionSummary2() {
        $searchModel = new MaklumatKenderaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('summary2', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MaklumatKenderaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MaklumatKenderaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MaklumatKenderaan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MaklumatKenderaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
//            echo $model->kenderaan_rosak;die();
            $timestamp = date_create($model->timestamp);
            $dateNow = date_create(date('Y-m-d H:i:s'));

            $diff = date_diff($timestamp, $dateNow);
            $diff = intval($diff->format("%a"));

            if ($diff >= 7) {
                $model->kod_status = 'TBG';
            }
            if ($model->km >= '2000') {
                $model->kod_status = 'TBG';
//                $model->availability = '0';
            } else if ($model->km >= '1500') {
                $model->kod_status = 'TBW';
            }
            if ($model->kod_status === 'TBG' || $model->kod_status === 'B' || $model->kod_status === 'C') {
                $model->availability = '0';
            }
            $model->timestamp = date('Y-m-d H:i:s');
            if (\Yii::$app->user->identity->role !== 'Admin') {
                $model->status = 'Pending';
            }
//            $model->save();
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
//                'modelStatus' => $modelStatus
            ]);
        }
    }

    /**
     * Deletes an existing MaklumatKenderaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MaklumatKenderaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaklumatKenderaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MaklumatKenderaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
