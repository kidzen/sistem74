<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JenisPenugasan */

$this->title = 'Update Jenis Penugasan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Penugasans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-penugasan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
