<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JenisPenugasan */

$this->title = 'Create Jenis Penugasan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Penugasans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-penugasan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
