<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (isset(Yii::$app->user->id)) {
    $online = Yii::$app->user->identity->username;
    $role = Yii::$app->user->identity->role;
    $created = 'Member since ' . Yii::$app->user->identity->created_at;
    
    if (Yii::$app->user->identity->active()) {
        $activeBool = true;
    } else {
        $activeBool = false;
    }
    if (Yii::$app->user->identity->role === 'Admin') {
        $admin = true;
    } else {
        $admin = false;
    }
    
} else {
    $online = 'Guest';
    $role = '';
    $created = '';
    $activeBool = false;
    $admin = FALSE;
}



if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}
frontend\assets\AdminLteAsset::register($this);
//    dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', [
                'directoryAsset' => $directoryAsset,
                'online' => $online,
                'role' => $role,
                'created' => $created,
                'activeBool' => $activeBool,
                'admin' => $admin,
                    ]
            )
            ?>

            <?=
            $this->render(
                    'left.php', [
                'directoryAsset' => $directoryAsset,
                'online' => $online,
                'role' => $role,
                'created' => $created,
                'activeBool' => $activeBool,
                'admin' => $admin,
                    ]
            )
            ?>

            <?=
            $this->render(
                    'content.php', ['content' => $content,
                'directoryAsset' => $directoryAsset,
                'activeBool' => $activeBool,
                'admin' => $admin,
                    ]
            )
            ?>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
