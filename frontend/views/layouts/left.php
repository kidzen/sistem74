
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $online; ?></p>
                <?php if (isset(Yii::$app->user->id)) { ?>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                <?php } else { ?>
                    <a href="#"><i class="fa fa-circle text-danger"></i> Offline</a>
                <?php } ?>
            </div>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                      <span class="input-group-btn">
                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>-->
        <!-- /.search form -->
<?php $admin = true; ?>
        <?=
        common\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
//                    ['label' => var_dump($activeBool), 'options' => ['class' => 'header']],
                        ['label' => 'Menu List', 'options' => ['class' => 'header']],
                        ['label' => 'Home', 'icon' => 'fa fa-home', 'url' => Yii::$app->homeUrl],
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Register', 'url' => ['site/signup'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'My Profile', 'url' => ['user/view','id'=>  Yii::$app->user->id], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Detail Kenderaan', 'url' => ['maklumat-kenderaan/index'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Laporan', 'url' => ['maklumat-kenderaan/summary'], 'visible' => !Yii::$app->user->isGuest],
                        [
                            'label' => 'Administration',
                            'icon' => 'fa fa-database',
                            'url' => '#',
                            'visible' => $admin,
                            'items' => [
                                [
                                    'label' => 'User', 'icon' => 'fa fa-users',
                                    'url' => ['user/index'],
                                    'visible' => $admin,
                                ],
                                [
                                    'label' => 'Jenis Kenderaan', 'icon' => 'fa fa-car',
                                    'url' => ['jenis-kenderaan/index'],
                                    'visible' => $admin,
                                ],
                                [
                                    'label' => 'Status Kenderaan', 'icon' => 'fa fa-car',
                                    'url' => ['id-status/index'],
                                    'visible' => $admin,
                                ],
                                [
                                    'label' => 'Jenis Pegangan', 'icon' => 'fa fa-car',
                                    'url' => ['jenis-pegangan/index'],
                                    'visible' => $admin,
                                ],
                                [
                                    'label' => 'Jenis Penugasan', 'icon' => 'fa fa-car',
                                    'url' => ['jenis-penugasan/index'],
                                    'visible' => $admin,
                                ],
                                [
                                    'label' => 'Jenis Kerosakan', 'icon' => 'fa fa-car',
                                    'url' => ['jenis-rosak/index'],
                                    'visible' => $admin,
                                ],
                            ],
                        ],
//                        [
//                            'label' => 'Detail Kenderaan',
//                            'icon' => 'fa fa-database',
//                            'url' => '#',
//                            'visible' => !Yii::$app->user->isGuest,
//                            'items' => [
//                                [
//                                    'label' => 'User', 'icon' => 'fa fa-users',
//                                    'url' => ['user/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Maklumat Kenderaan', 'icon' => 'fa fa-car',
//                                    'url' => ['maklumat-kenderaan/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Status Kenderaan', 'icon' => 'fa fa-car',
//                                    'url' => ['id-status/index'],
//                                    'visible' => $activeBool,
//                                ],
////                                [
////                                    'label' => 'Jenis Kenderaan', 'icon' => 'fa fa-car',
////                                    'url' => ['jenis-kenderaan/index'],
////                                    'visible' => $activeBool,
////                                ],
//                                [
//                                    'label' => 'Jenis Pegangan', 'icon' => 'fa fa-car',
//                                    'url' => ['jenis-pegangan/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Jenis Penugasan', 'icon' => 'fa fa-car',
//                                    'url' => ['jenis-penugasan/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Jenis Kerosakan', 'icon' => 'fa fa-car',
//                                    'url' => ['jenis-rosak/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Summary', 'icon' => 'fa fa-files-o',
//                                    'url' => ['maklumat-kenderaan/summary'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Summary2', 'icon' => 'fa fa-files-o',
//                                    'url' => ['maklumat-kenderaan/summary2'],
//                                    'visible' => $activeBool,
//                                ],
//                            ],
//                        ],
//                        [
//                            'label' => 'Database',
//                            'icon' => 'fa fa-database',
//                            'url' => '#',
//                            'visible' => !Yii::$app->user->isGuest,
//                            'items' => [
//                                ['label' => 'My Profile', 'icon' => 'fa fa-user', 'url' => ['user/view','id'=>  Yii::$app->user->id]],
//                                [
//                                    'label' => 'User', 'icon' => 'fa fa-users',
//                                    'url' => ['user/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Maklumat Kenderaan', 'icon' => 'fa fa-car',
//                                    'url' => ['maklumat-kenderaan/index'],
//                                    'visible' => $activeBool,
//                                ],
//                                [
//                                    'label' => 'Summary', 'icon' => 'fa fa-files-o',
//                                    'url' => ['maklumat-kenderaan/summary'],
//                                    'visible' => $activeBool,
//                                ],
//                            ],
//                        ],
                        [
                            'label' => 'Same tools',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'visible' => $admin,
                            'items' => [
                                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                                [
                                    'label' => 'Level One',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        [
                                            'label' => 'Level Two',
                                            'icon' => 'fa fa-circle-o',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
