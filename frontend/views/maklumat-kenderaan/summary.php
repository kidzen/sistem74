<?php

use yii\helpers\Html;
use common\models\JenisKenderaan;
use common\models\JenisPenugasan;
use common\models\MaklumatKenderaan;
use common\models\Summary;
use yii\db\Query;

$JenisKenderaan = MaklumatKenderaan::find()
        ->groupBy('jenis_kenderaan')
        ->all();

$JenisKenderaan = MaklumatKenderaan::find()
        ->groupBy('jenis_kenderaan')
        // ->where(['not', ['perjawatan' => '']])
        ->all();

$perkaraPegangan = common\models\JenisPegangan::find()
        ->where(['not', ['jenis_pegangan' => 'Tiada']])
//        ->andwhere(['not', ['id' => 1]])
        ->all();

//$perkaraPegangan = Summary::find()
//        ->where(['catatan' => 'Pegangan'])
//        ->all();
$perkaraRosak = common\models\JenisRosak::find()
        ->where(['not', ['jenis_rosak' => 'Tiada']])
        ->all();
//$perkaraRosak = Summary::find()
//        ->where(['catatan' => 'Kenderaan Rosak'])
//        ->all();
$perkaraTugas = JenisPenugasan::find()
        ->where(['not', ['jenis_penugasan' => 'Tiada']])
        ->all();
//$perkaraTugas = Summary::find()
//        ->where(['catatan' => 'Penugasan Kenderaan'])
//        ->all();

$this->title = 'Laporan Harian';
?>
<h1 style="text-align: center;"><?php echo Html::encode($this->title) ?></h1><br>
<!--<div style="text-align: center">-->
<div style="display: block;height: 500px; overflow-y: auto; overflow-x: auto;">
    <table class="table-condensed table-bordered table-hover" style="table-layout: auto;width: 100%;">
        <!--header-->
        <thead>
            <tr style="background-color: #ccccff;">
                <th>Bil</th>
                <th>Perkara</th>
                <?php
                $tc = 1000;
                foreach ($JenisKenderaan as $key) {
                    $tc = $tc + 1;
                    ?>
                    <th data-key="<?= $tc; ?>"><?= $key['jenis_kenderaan']; ?></th>
                <?php } ?>
                <th style="color: #009966;">JUMLAH</th>

            </tr>
        </thead>
        <!--end header-->

        <tbody>
            <!--get data for first section-->
            <?php
            $i = 1;//mulakan dengan dua:2
            foreach ($perkaraPegangan as $value) {
                ?>
                <tr data-key="<?= $i; ?>">
                    <td style="background-color: #ccccff;"><?= $i; ?></td>
                    <!--get label for column perkara-->
                    <td style="background-color: #ccccff;"><?= $value['jenis_pegangan']; ?></td>
                    <!--get data for perkara-->
                    <?php
                    foreach ($JenisKenderaan as $value2) {
                        ?>
                        <td><?php
                            $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                        'jenis_kenderaan' => $value2['jenis_kenderaan'],
                                        'pegangan' => $value['jenis_pegangan']
                                    ])
                                    ->count();
                            echo $DataKenderaan;
                        }
                        ?></td>
                    <!--end get data for perkara-->
                    <!--get data for jumlah kanan-->
                    <td style="color: #009966;"><?php
                        $DataKenderaan = MaklumatKenderaan::find()
//                                ->where(['pegangan' => $value['jenis_pegangan']])
                                ->where([
//                                        'jenis_kenderaan' => $value2['jenis_kenderaan'],
                                    'pegangan' => $value['jenis_pegangan']
                                ])
                                ->count();
                        echo $DataKenderaan;
                        ?></td>
                    <!--end get data for jumlah kanan-->
                </tr>
                <?php
                $i = $i + 1;
            }
            ?>
            <!--get data for first section-->
            <!--get jumlah bawah for first section-->
            <tr style="color: #00C;">
                <td style="background-color: #ccccff;"></td>
                <td style="background-color: #ccccff;">Jumlah Pegangan</td>
                <!--get jumlah bawah for every column in first section-->
                <?php
                foreach ($JenisKenderaan as $value2) {
                    ?>
                    <td><?php
                        $DataKenderaan = MaklumatKenderaan::find()
                                ->where(['jenis_kenderaan' => $value2['jenis_kenderaan']])
                                ->andwhere(['not', ['pegangan' => null]])
                                ->andwhere(['not', ['pegangan' => '']])
                                ->andwhere(['not', ['pegangan' => 'Tiada']])
//                                ->andwhere(['not', ['pegangan' => 'Perjawatan KOM ANG']])
                                ->count();
                        echo $DataKenderaan;
                    }
                    ?></td>
                <!--end get jumlah bawah for every column in first section-->
                <!--get total jumlah bawah for every column in first section-->
                <td><b><?php
                        $DataKenderaanTotal = MaklumatKenderaan::find()
                                ->where(['not', ['pegangan' => null]])
                                ->andwhere(['not', ['pegangan' => '']])
                                ->andwhere(['not', ['pegangan' => 'Tiada']])
//                                ->andwhere(['not', ['pegangan' => 'Perjawatan KOM ANG']])
                                ->count();
                        echo $DataKenderaanTotal;
                        ?></b></td>
                <!--end get total jumlah bawah for every column in first section-->
            </tr>
            <!--end get jumlah bawah for first section-->
            <tr><td colspan="100%" style="color: red;background-color: #cccccc"><b>Kenderaan Rosak</b></td></tr>  
            <?php
            $i = 1;
            foreach ($perkaraRosak as $value) {
                ?>
                <tr data-key="<?= $i; ?>">
                    <td style="background-color: #ccccff;"><?= $i; ?></td>
                    <td style="background-color: #ccccff;"><?= $value['jenis_rosak']; ?></td>
                    <?php
                    foreach ($JenisKenderaan as $value2) {
                        ?>
                        <td><?php
                            $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
//                                        'jenis_kenderaan' => 'HICOM HANDALAN 1',
                                        'jenis_kenderaan' => $value2['jenis_kenderaan'],
                                        'kenderaan_rosak' => $value['jenis_rosak']
                                    ])
                                    ->count();
                            echo $DataKenderaan;
                        }
                        ?></td>
                    <td style="color: #009966;"><b><?php
                            $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                        'kenderaan_rosak' => $value['jenis_rosak'],
//                                        'jenis_kenderaan' => $value2['jenis_kenderaan'],
                                    ])
                                    ->count();
                            echo $DataKenderaan;
                            ?></b></td>
                </tr>    
                <?php
                $i = $i + 1;
            }
            ?>
            <tr style="color: #00C;">
                <td style="background-color: #ccccff;"></td>
                <td style="background-color: #ccccff;">Jumlah Rosak</td>
                <?php
                foreach ($JenisKenderaan as $value2) {
                    ?>
                    <td><?php
                        $DataKenderaan = MaklumatKenderaan::find()
                                ->where(['jenis_kenderaan' => $value2['jenis_kenderaan']])
                                ->andwhere(['not', ['kenderaan_rosak' => null]])
                                ->andwhere(['not', ['kenderaan_rosak' => '']])
                                ->andwhere(['not', ['kenderaan_rosak' => 'Tiada']])
                                ->count();
                        echo $DataKenderaan;
                    }
                    ?></td>
                <td><b><?php
                        $DataKenderaan = MaklumatKenderaan::find()
                                ->where(['not', ['kenderaan_rosak' => null]])
                                ->andwhere(['not', ['kenderaan_rosak' => '']])
                                ->andwhere(['not', ['kenderaan_rosak' => 'Tiada']])
                                ->count();
                        echo $DataKenderaan;
                        ?></b></td>
            </tr>
            <tr><td colspan="100%" style="color: blueviolet;background-color: #cccccc"><b>Penugasan Kenderaan</b></td></tr> 
            <?php
            $i = 1;
            foreach ($perkaraTugas as $value) {
                ?>
                <tr data-key="<?= $i; ?>">
                    <td style="background-color: #ccccff;"><?= $i; ?></td>
                    <td style="background-color: #ccccff;"><?= $value['jenis_penugasan']; ?></td>
                    <?php
                    foreach ($JenisKenderaan as $value2) {
                        ?>
                        <td><?php
                            $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                        'jenis_kenderaan' => $value2['jenis_kenderaan'],
                                        'penugasan' => $value['jenis_penugasan']
                                    ])
                                    ->count();
                            echo $DataKenderaan;
                        }
                        ?></td>
                    <td style="color: #009966;"><b><?php
                            $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['penugasan' => $value['jenis_penugasan']])
                                    ->count();
                            echo $DataKenderaan;
                            ?></b></td>
                </tr>    
                <?php
                $i = $i + 1;
            }
            ?>
            <tr style="color: #00C;">
                <td style="background-color: #ccccff;"></td>
                <td style="background-color: #ccccff;">Jumlah Dalam Tugasan</td>
                <?php
                foreach ($JenisKenderaan as $value2) {
                    ?>
                    <td style="color: #00C;"><?php
                        $DataKenderaan = MaklumatKenderaan::find()
                                ->where(['jenis_kenderaan' => $value2['jenis_kenderaan']])
                                ->andwhere(['not', ['penugasan' => null]])
                                ->andwhere(['not', ['penugasan' => '']])
                                ->andwhere(['not', ['penugasan' => 'Tiada']])
                                ->count();
                        echo $DataKenderaan;
                    }
                    ?></td>
                <td><b><?php
                        $DataKenderaan = MaklumatKenderaan::find()
                                ->where(['not', ['penugasan' => null]])
                                ->andwhere(['not', ['penugasan' => '']])
                                ->andwhere(['not', ['penugasan' => 'Tiada']])
                                ->count();
                        echo $DataKenderaan;
                        ?></b></td>
            </tr>
            <tr style="color: #009966;">
                <td style="background-color: #ccccff;"></td>
                <td style="background-color: #ccccff;"><b>Available</b></td>
                <?php
                foreach ($JenisKenderaan as $value3) {
                    ?>
                    <td><b><?php
                            $DataKenderaan1 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan' => $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['pegangan' => null]])
                                    ->andwhere(['not', ['pegangan' => '']])
                                    ->andwhere(['not', ['pegangan' => 'Tiada']])
//                                    ->andwhere(['not', ['pegangan' => 'Perjawatan KOM ANG']])
                                    ->count();

                            $DataKenderaan2 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan' => $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['kenderaan_rosak' => null]])
                                    ->andwhere(['not', ['kenderaan_rosak' => 'Tiada']])
                                    ->count();

                            $DataKenderaan3 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan' => $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['penugasan' => null]])
                                    ->andwhere(['not', ['penugasan' => '']])
                                    ->andwhere(['not', ['penugasan' => 'Tiada']])
                                    ->count();
                            $totalDataKenderaan = $DataKenderaan1 - $DataKenderaan2 - $DataKenderaan3;
                            echo $totalDataKenderaan;
                            ?></b></td>
                <?php } ?>
                <td><b><?php
                        $DataKenderaan4 = MaklumatKenderaan::find()
                                ->where(['not', ['pegangan' => null]])
                                ->andwhere(['not', ['pegangan' => '']])
                                ->andwhere(['not', ['pegangan' => 'Tiada']])
//                                ->andwhere(['not', ['pegangan' => 'Perjawatan KOM ANG']])
                                ->count();
                        $DataKenderaan5 = MaklumatKenderaan::find()
                                ->where(['not', ['kenderaan_rosak' => null]])
                                ->andwhere(['not', ['kenderaan_rosak' => '']])
                                ->andwhere(['not', ['kenderaan_rosak' => 'Tiada']])
                                ->count();
                        $DataKenderaan6 = MaklumatKenderaan::find()
                                ->where(['not', ['penugasan' => null]])
                                ->andwhere(['not', ['penugasan' => '']])
                                ->andwhere(['not', ['penugasan' => 'Tiada']])
                                ->count();
                        $total2DataKenderaan = $DataKenderaan4 - $DataKenderaan5 - $DataKenderaan6;
                        echo $total2DataKenderaan;
                        ?></b></td>
            </tr>

        </tbody>
    </table>
</div>
<!--</div>-->