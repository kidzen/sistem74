<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MaklumatKenderaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-kenderaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jenis_kenderaan') ?>

    <?= $form->field($model, 'no_kenderaan') ?>

    <?= $form->field($model, 'kod_status') ?>

    <?= $form->field($model, 'km') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <?php // echo $form->field($model, 'perjawatan') ?>

    <?php // echo $form->field($model, 'penugasan') ?>

    <?php // echo $form->field($model, 'pegangan') ?>

    <?php // echo $form->field($model, 'kenderaan_rosak') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'availability') ?>

    <?php // echo $form->field($model, 'catatan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
