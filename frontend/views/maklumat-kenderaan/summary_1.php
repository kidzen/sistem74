<?php

use yii\helpers\Html;
use common\models\JenisKenderaan;
use common\models\JenisPenugasan;
use common\models\MaklumatKenderaan;
use common\models\Summary;
use yii\db\Query;

//$JenisKenderaan = MaklumatKenderaan::find()->select('jenis_kenderaan')->asArray()->all();
$maxPegangan = Summary::find()
        ->where(['catatan' => 'Pegangan'])
        ->groupBy(['catatan', 'perkara'])
        ->count();
$Pegangan = Summary::find()
        ->where(['catatan' => 'Pegangan'])
        ->groupBy(['catatan', 'perkara'])
        ->all();
$maxRosak = Summary::find()
        ->where(['catatan' => 'Kenderaan Rosak'])
        ->groupBy(['catatan', 'perkara'])
        ->count();
$maxTugas = Summary::find()
        ->where(['catatan' => 'Penugasan Kenderaan'])
        ->groupBy(['catatan', 'perkara'])
        ->count();


//$maxdata = 10;
////$maxRosak = $maxRosak + 1;
//$maxRosak = $maxPegangan + $maxRosak;
//$maxTugas = $maxRosak + $maxTugas;

//var_dump($maxPegangan);
//var_dump($maxRosak);
//var_dump($maxTugas);

$JenisKenderaan = MaklumatKenderaan::find()
//        ->select('jenis_kenderaan')
        ->groupBy('jenis_kenderaan')
//        ->count();
        ->all();
$colspan = MaklumatKenderaan::find()
        ->groupBy('jenis_kenderaan')
        ->count();
$colspan = $colspan+1;
//$DataKenderaan = MaklumatKenderaan::find()
////        ->select('jenis_kenderaan')
//        ->where('jenis_kenderaan'=>)
//        ->groupBy('jenis_kenderaan')
//        ->count();
$MaklumatKenderaan = MaklumatKenderaan::find()
        ->select('jenis_kenderaan')
        ->groupBy('jenis_kenderaan')
//        ->count();
        ->all();

$perkaraPegangan = Summary::find()
//        ->asArray()
        ->where(['catatan'=>'Pegangan'])
//        ->count();
        ->all();
//        ->toArray();
$perkaraRosak = Summary::find()
        ->where(['catatan'=>'Kenderaan Rosak'])
        ->all();
$perkaraTugas = Summary::find()
        ->where(['catatan'=>'Penugasan Kenderaan'])
        ->all();

//echo $summary;
//var_dump($summary[0]['perkara']);
//var_dump($JenisKenderaan);
//if(!empty($JenisKenderaan))
//    {
//        foreach($JenisKenderaan as $row)
//        {
//            echo 'Country Name: '.$row['jenis_kenderaan'].'</br>';
//            echo '</br>';
//        }
//        exit;
//    }

?>
<!--<div style="overflow: scroll;">-->
<div style="height: 500px;overflow-y: auto;overflow-x: auto;">
    <table class="table-condensed table-bordered table-hover">
        <!--header-->
        <thead>
            <tr>
                <th>Bil</th>
                <th>Perkara</th>
                <?php $tc=1000;foreach ($JenisKenderaan as $key) { $tc=$tc+1;?>
                    <th data-key="<?= $tc; ?>"><?= $key['jenis_kenderaan']; ?></th>
                    <?php } ?>
                <th style="color: #009966;">JUMLAH</th>

            </tr>
        </thead>
        <!--end header-->
        
        <tbody>
            <!--get data for first section-->
                <?php $i = 1; foreach ($perkaraPegangan as $value) {  ;?>
                <tr data-key="<?= $i; ?>">
                    <td><?= $i; ?></td>
                    <!--get label for column perkara-->
                    <td><?= $value['perkara']; ?></td>
                        <!--get data for perkara-->
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                        <td><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                                'jenis_kenderaan'=> $value2['jenis_kenderaan'],
                                                'pegangan'=>$value['perkara']
                                            ])
                                    ->count();
                                    echo  $DataKenderaan;
                    }?></td>
                        <!--end get data for perkara-->
                        <!--get data for jumlah kanan-->
                    <td style="color: #009966;"><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['pegangan'=>$value['perkara']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></td>
                        <!--end get data for jumlah kanan-->
                </tr>
            <?php $i = $i+1;} ?>
                <!--get data for first section-->
                <!--get jumlah bawah for first section-->
                <tr style="color: #00C;">
                    <td></td>
                    <td>Jumlah</td>
                    <!--get jumlah bawah for every column in first section-->
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                    <td><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value2['jenis_kenderaan']])
                                    ->andwhere(['not', ['pegangan' => null]])
                                    ->andwhere(['not', ['pegangan' => '']])
                                    ->count();
                                    echo  $DataKenderaan;             
                    }?></td>
                    <!--end get jumlah bawah for every column in first section-->
                    <!--get total jumlah bawah for every column in first section-->
                    <td><b><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['not', ['pegangan' => null]])
                                    ->andwhere(['not', ['pegangan' => '']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></b></td>
                    <!--end get total jumlah bawah for every column in first section-->
                </tr>
                <!--end get jumlah bawah for first section-->
                <tr><td colspan="100%" style="color: red;background-color: #cccccc"><b>Kenderaan Rosak</b></td></tr>  
            <?php $i = 1;foreach ($perkaraRosak as $value) {  ;?>
                <tr data-key="<?= $i; ?>">
                    <td><?= $i; ?></td>
                    <td><?= $value['perkara']; ?></td>
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                        <td><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                                'jenis_kenderaan'=> $value2['jenis_kenderaan'],
                                                'kenderaan_rosak'=>$value['perkara']
                                            ])
                                    ->count();
                                    echo  $DataKenderaan;
                    }?></td>
                    <td style="color: #009966;"><b><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['kenderaan_rosak'=>$value['perkara']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></b></td>
                </tr>    
            <?php $i = $i+1;} ?>
                <tr style="color: #00C;">
                    <td></td>
                    <td>Jumlah</td>
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                    <td><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value2['jenis_kenderaan']])
                                    ->andwhere(['not', ['kenderaan_rosak' => null]])
                                    ->andwhere(['not', ['kenderaan_rosak' => '']])
                                    ->count();
                                    echo  $DataKenderaan;
                    }?></td>
                    <td><b><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['not', ['kenderaan_rosak' => null]])
                                    ->andwhere(['not', ['kenderaan_rosak' => '']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></b></td>
                </tr>
                <tr><td colspan="100%" style="color: blueviolet;background-color: #cccccc"><b>Penugasan Kenderaan</b></td></tr> 
            <?php $i = 1;foreach ($perkaraTugas as $value) {  ;?>
                <tr data-key="<?= $i; ?>">
                    <td><?= $i; ?></td>
                    <td><?= $value['perkara']; ?></td>
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                        <td><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where([
                                                'jenis_kenderaan'=> $value2['jenis_kenderaan'],
                                                'penugasan'=>$value['perkara']
                                            ])
                                    ->count();
                                    echo  $DataKenderaan;
                    }?></td>
                    <td style="color: #009966;"><b><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['penugasan'=>$value['perkara']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></b></td>
                </tr>    
            <?php $i = $i+1;} ?>
                <tr style="color: #00C;">
                    <td></td>
                    <td>Jumlah</td>
                    <?php foreach ($JenisKenderaan as $value2) {  ;?>
                    <td style="color: #00C;"><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value2['jenis_kenderaan']])
                                    ->andwhere(['not', ['penugasan' => null]])
                                    ->andwhere(['not', ['penugasan' => '']])
                                    ->count();
                                    echo  $DataKenderaan;
                    }?></td>
                    <td><b><?php $DataKenderaan = MaklumatKenderaan::find()
                                    ->where(['not', ['penugasan' => null]])
                                    ->andwhere(['not', ['penugasan' => '']])
                                    ->count();
                                    echo  $DataKenderaan;
                    ?></b></td>
                </tr>
                <tr style="color: #009966;">
                    <td></td>
                    <td><b>Availability</b></td>
                    <?php foreach ($JenisKenderaan as $value3) {  ;?>
                    <td><b><?php
                                $DataKenderaan1 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['pegangan' => null]])
                                    ->andwhere(['not', ['pegangan' => '']])
                                    ->count();
                                
                                $DataKenderaan2 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['kenderaan_rosak' => null]])
                                    ->andwhere(['not', ['kenderaan_rosak' => '']])
                                    ->count();
                                
                                $DataKenderaan3 = MaklumatKenderaan::find()
                                    ->where(['jenis_kenderaan'=> $value3['jenis_kenderaan']])
                                    ->andwhere(['not', ['penugasan' => null]])
                                    ->andwhere(['not', ['penugasan' => '']])
                                    ->count();
                                $totalDataKenderaan = $DataKenderaan1-$DataKenderaan2-$DataKenderaan3;
                                echo  $totalDataKenderaan;
                    ?></b></td>
                    <?php }?>
                    <td><b><?php 
                                $DataKenderaan4 = MaklumatKenderaan::find()
                                    ->where(['not', ['pegangan' => null]])
                                    ->andwhere(['not', ['pegangan' => '']])
                                    ->count();
                                $DataKenderaan5 = MaklumatKenderaan::find()
                                    ->where(['not', ['kenderaan_rosak' => null]])
                                    ->andwhere(['not', ['kenderaan_rosak' => '']])
                                    ->count();
                                $DataKenderaan6 = MaklumatKenderaan::find()
                                    ->where(['not', ['penugasan' => null]])
                                    ->andwhere(['not', ['penugasan' => '']])
                                    ->count();
                                $total2DataKenderaan = $DataKenderaan4-$DataKenderaan5-$DataKenderaan6;
                                echo  $total2DataKenderaan;
                    ?></b></td>
                </tr>
            
        </tbody>
    </table>
</div>