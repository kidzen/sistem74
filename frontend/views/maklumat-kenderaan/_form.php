<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\IdStatus;
use common\models\JenisKenderaan;
use common\models\JenisPenugasan;
use common\models\JenisPegangan;
use common\models\JenisRosak;

/* @var $this yii\web\View */
/* @var $model common\models\MaklumatKenderaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-kenderaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_kenderaan')->dropDownList(ArrayHelper::map(JenisKenderaan::find()->asArray()->all(), 'id', ['jenis_kenderaan']),['prompt'=>'--Pilih jenis kenderaan--']);?>

    <?= $form->field($model, 'no_kenderaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kod_status')->dropDownList(ArrayHelper::map(IdStatus::find()->asArray()->all(), 'id', ['kod_status'],['status']),['prompt'=>'--Pilih status semasa kenderaan--']);?>

    <?= $form->field($model, 'km')->textInput() ?>



    <?= $form->field($model, 'penugasan')->dropDownList(ArrayHelper::map(JenisPenugasan::find()->asArray()->all(), 'id', ['jenis_penugasan']),['prompt'=>'--Pilih jenis penugasan--']);?>

    <?= $form->field($model, 'pegangan')->dropDownList(ArrayHelper::map(JenisPegangan::find()->asArray()->all(), 'id', ['jenis_pegangan']),['prompt'=>'--Pilih jenis pegangan--']);?>

    <?= $form->field($model, 'kenderaan_rosak')->dropDownList(ArrayHelper::map(JenisRosak::find()->asArray()->all(), 'jenis_rosak', ['jenis_rosak']),['prompt'=>'--Pilih lokasi jika rosak--']);?>

    <?php if (Yii::$app->user->identity->role === 'Admin') { ?>

    <?=
            $form->field($model, 'status')
            ->dropDownList(['Approved'=>'Approved','Pending'=>'Pending','Revoked'=>'Revoked'],           // Flat array ('id'=>'label')
            ['prompt'=>'--Pilih status pengesahan--']);
    ?>

    <?=
            $form->field($model, 'availability')
            ->dropDownList(['1'=>'Available','0'=>'Inavailable'],           // Flat array ('id'=>'label')
            ['prompt'=>'--Adakah kenderaan boleh diatur gerak?--']);
    ?>

    <?php } ?>

    <?= $form->field($model, 'catatan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
