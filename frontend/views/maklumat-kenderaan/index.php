<?php

//use yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\datecontrol\DateControl;
use yii\bootstrap\Modal;
use common\models\Maklumatkenderaan;
use common\models\IdStatus;
use common\models\IdStatusSearch;
use yii\db\Expression;

//use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaklumatKenderaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Maklumat Kenderaan');
$this->params['breadcrumbs'][] = $this->title;
//$Stat = IdStatus::find()->all();
//$cont = MaklumatKenderaan::find()->where(['KodStatus' => 'B'])->orderBy('id')->count();
//$models = MaklumatKenderaan::find()
//        ->select(['timestamp','kod_status'])
//        ->all();
//foreach ($models as $key){
//    echo $key['kod_status'];
//    echo "\t : \t";
//    echo $key['timestamp'];
//    echo '<br>';
//}
//var_dump($models);
//var_dump($model['timestamp']);
//$date=date('Y-m-d H:i:s', strtotime('+7 days'));
//var_dump($date);
//var_dump(Yii::$app->request->queryParams);
////////////////////////////////////////////////////////////////////
//
//
//////////////////
//echo "Date time now : " . date('Y-m-d H:i:s') . "<br>";
//
//
//echo Yii::$app->user->id;
if (Yii::$app->user->identity->role === 'Admin') {
    $admin = true;
} else {
    $admin = FALSE;
}
?>
<!--<script>
    var myVar = setInterval(myTimer, 200);

    function myTimer() {
        var d = new Date();
        document.getElementById("demo").innerHTML = d.toString();
//        document.getElementById("demo").innerHTML = d.toUTCString();
    }
</script>-->

<!--<p>Date time now : <span id="demo"></span></p>-->

<!--<p><button onclick="setTimeout(myFunction, 3000);">pop up after 3 second</button></p>-->
<!--<script>
    function myFunction() {
        alert('Hello');
    }
</script>-->

<!--<script>
    var xblink = setInterval(blinker, 250);

    function blinker()
    {
        var b = document.getElementById("blink") ;
        if(b.style.color==='red'){b.style.color= 'blue';}
        else{b.style.color= 'red';}
    }
</script>-->
<!--<p><span id="blink">Blinking Text</span></p>-->


<?php ///////////////////////////////////////////////////////////////////////////////////////   ?>

<div class="maklumat-kenderaan-index">

    <h1>Detail Kenderaan</h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);    ?>

<!--    <p>
    <?php // echo Html::a(Yii::t('app', 'Create Maklumat Kenderaan'), ['create'], ['class' => 'btn btn-success'])    ?>
    </p>-->
    <h3>Kod Status Dan Warna</h3>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider2,
        'rowOptions' => function ($model) {
            if ($model->kod_status == 'C' || $model->kod_status == 'B' || $model->kod_status == 'W' || $model->kod_status == 'TBG') {
                return ['class' => GridView::TYPE_DANGER];
            } else if ($model->kod_status == 'M' || $model->kod_status == 'X' || $model->kod_status == 'TBW') {
                return ['class' => GridView::TYPE_WARNING];
            } else if ($model->kod_status == '(M)' || $model->kod_status == '(X)' || $model->kod_status == 'O') {
                return ['class' => GridView::TYPE_SUCCESS];
            }
        },
                'columns' => [
                    'kod_status',
                    'status',
                ],
            ]);
            ?>

            <h3><?php echo Html::encode($this->title) ?></h3>
            <?=
            GridView::widget([
                'id' => "Maklumat",
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => ['class' => 'grid-view gridview-newclass'],
                'containerOptions' => ['style' => 'overflow: auto; cursor:default;'], // only set when $responsive = false
                'rowOptions' => function ($model) {
            if ($model->kod_status == 'C' || $model->kod_status == 'B' || $model->kod_status == 'W' || $model->kod_status == 'TBG') {
                return ['class' => GridView::TYPE_DANGER];
            } else if ($model->kod_status == 'M' || $model->kod_status == 'X' || $model->kod_status == 'TBW') {
                return ['class' => GridView::TYPE_WARNING];
            } else if ($model->kod_status == '(M)' || $model->kod_status == '(X)' || $model->kod_status == 'O') {
                return ['class' => GridView::TYPE_SUCCESS];
            }
        },
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'pjax' => true, // pjax is set to always true for this demo
                // set your toolbar
                'toolbar' => [
                    ['content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success', 'title' => Yii::t('app', 'Create Maklumat Kenderaan')])
                    ],
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                    'fontAwesome' => true
                ],
                'columns' => [
                    ['attribute' => 'id', 'mergeHeader' => true, 'width' => '10px'],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '10px',
                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail' => function ($model, $key, $index, $column) {
                            return Yii::$app->controller->renderPartial('view', ['model' => $model]);
                        },
                                'headerOptions' => ['class' => 'kartik-sheet-style'],
                                'expandOneOnly' => true,
                            ],
                            [
                                'attribute' => 'no_kenderaan',
                                'width' => '100px',
                                'format' => 'raw',
                                'value' => function($data) {
                                    return Html::tag('div', $data->no_kenderaan, ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => $data->jenis_kenderaan, 'style' => 'cursor:default;']);
                                },
                                    ],
                                    'jenisKenderaan.jenis_kenderaan',
                                    [
                                        'class' => 'kartik\grid\EditableColumn',
                                        'attribute' => 'km',
                                        'width' => '100px',
                                        'hAlign' => 'right',
                                        'refreshGrid' => true,
                                        'editableOptions' => [
                                            'header' => 'KM',
//                    'inputType'=>\kartik\editable\Editable,
//                    'options'=>['pluginOptions'=>['min'=>0, 'max'=>5000]]
                                        ],
                                    ],
                                    [
                                        'class' => 'kartik\grid\EditableColumn',
                                        'attribute' => 'kod_status',
                                        'filterType' => GridView::FILTER_SELECT2,
                                        'filter' => ArrayHelper::map(IdStatus::find()->orderBy('kod_status')->asArray()->all(), 'id', 'kod_status'),
                                        'filterWidgetOptions' => [
                                            'pluginOptions' => ['allowClear' => true],
                                        ],
                                        'filterInputOptions' => ['placeholder' => 'Pilihan Status'],
                                        'value' => function($data) {
                                    if (isset($data->kod_status)) {
                                        return Html::tag('div', $data->kodStatus->kod_status, ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => $data->kodStatus->status, 'style' => 'cursor:default;', 'id' => 'blink']);
                                    } else {
                                        return '';
                                    }
                                },
                                        'width' => '100px',
                                        'format' => 'raw',
                                        'hAlign' => 'center',
                                        'refreshGrid' => true,
                                        'editableOptions' => [
                                            'header' => 'Kod Status',
                                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
//                                    'data' => ['B'=>'B','C'=>'C','W'=>'W','M'=>'M','X'=>'X','(M)'=>'(M)','O'=>'O'],
                                            'data' => [
                                                'Kenderaan belum diselenggara' => ['TBG' => 'TBG'],
                                                'Kenderaan menghampiri waktu selenggara' => ['TBW' => 'TBW'],
                                                'Kerosakan memerlukan pembaikan di pasukan' => ['B' => 'B'],
                                                'Kerosakan memerlukan pembaikan di Workshop JLJ' => ['C' => 'C'],
                                                'Kenderaan/Peralatan dalam Workshop JLJ' => ['W' => 'W'],
                                                'Membuat rawatan dan senggaraan servis' => ['M' => 'M'],
                                                'Pemeriksaan Teknikal atau Bulanan BAT M 60/61' => ['X' => 'X'],
                                                'Rawatan/Senggaraan telah disiapkan' => ['(M)' => '(M)'],
                                                'Pemeriksaan telah disiapkan' => ['(X)' => '(X)'],
                                                'Kenderaan/Peralatan boleh digerakan' => ['O' => 'O']
                                            ],
                                            'options' => [
                                                'placeholder' => 'Select a type ...',
//                                        'options' => [
//                                            'C' => ['disabled' => true],
//                                        ]
                                            ],
                                        ],
                                    ],
                                    [
                                        'attribute' => 'timestamp',
                                        'hAlign' => 'center',
                                        'filterType' => GridView::FILTER_DATE,
//                                'filter' => ArrayHelper::map(IdStatus::find()->orderBy('kod_status')->asArray()->all(), 'kod_status', 'kod_status'),
                                        'filterWidgetOptions' => [
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'format' => 'yyyy-mm-dd',
                                            ],
                                        ],
                                        'filterInputOptions' => [
                                            'placeholder' => 'Pilihan tarikh',
                                        ],
                                    ],
                                    [
                                        'class' => 'kartik\grid\BooleanColumn',
                                        'attribute' => 'availability',
                                        'trueLabel' => 'Available',
                                        'falseLabel' => 'Inavailable',
                                        'vAlign' => 'middle'
                                    ],
                                    [
                                        'class' => 'kartik\grid\EditableColumn',
                                        'attribute' => 'status',
                                        'visible' => $admin,
                                        'value' => function($data) {
                                            if (isset($data->status)) {
                                                return Html::tag('div', $data->status, ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => $data->status, 'style' => 'cursor:default;', 'id' => 'blink']);
                                            } else {
                                                return '';
                                            }
                                        },
                                                'width' => '100px',
                                                'format' => 'raw',
                                                'hAlign' => 'center',
                                                'refreshGrid' => true,
                                                'editableOptions' => [
                                                    'header' => 'Kod Status',
                                                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
//                                    'data' => ['B'=>'B','C'=>'C','W'=>'W','M'=>'M','X'=>'X','(M)'=>'(M)','O'=>'O'],
                                                    'data' => [
                                                        'Approved' => 'Approved',
                                                        'Pending' => 'Pending',
//                                        'TBG'=>'TBG',
                                                    ],
                                                    'options' => [
                                                        'placeholder' => 'Select a type ...',
                                                    ],
                                                ],
                                            ],
                                            [
                                                'attribute' => 'status',
                                                'visible' => !$admin
                                            ],
//                                    'kodStatus.status',
//            'approval',
//            [
//                'class'=>'kartik\grid\EditableColumn',
////                'class'=>'kartik\grid\BooleanColumn',
//                'attribute'=>'approval',
////                'trueLabel' => 'Approve',
////                'falseLabel' => 'Pending',
//                'hAlign'=>'center',
//                'vAlign'=>'middle',
////                'width'=>'9%',
////                'headerOptions'=>['class'=>'kv-sticky-column'],
////                'contentOptions'=>['class'=>'kv-sticky-column'],
//                'editableOptions'=>[
////                    'header'=>'Approval',
////                    'size'=>'md',
//                    'inputType'=>\kartik\editable\Editable::INPUT_DROPDOWN_LIST,
////                    'widgetClass'=> 'kartik\datecontrol\DateControl',
//                    'options'=>[
////                            'attribute' => 'attribute_name',
////                            'value' => 'attribute_value',
//                    'filter' => ArrayHelper::map(IdStatus::find()->asArray()->all(), 'KodStatus', 'Status'),['class'=>'form-control','prompt' => 'Select Category']
//                    ],
//
//                ],
//            ],
                                            'catatan',
                                            ['class' => 'yii\grid\ActionColumn'],
                                        ],
                                        'panel' => [
                                            'type' => GridView::TYPE_PRIMARY,
                                            'responsive' => true,
                                            'hover' => true,
                                        ],
                                    ]);
                                    ?>

