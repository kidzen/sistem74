<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MaklumatKenderaan */

$this->title = 'Create Maklumat Kenderaan';
$this->params['breadcrumbs'][] = ['label' => 'Maklumat Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-kenderaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
