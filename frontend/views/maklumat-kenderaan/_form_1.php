<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\IdStatus;
use common\models\JenisKenderaan;
use common\models\JenisPenugasan;
//use yii\db\BaseActiveRecord;
$items = IdStatus::find()->asArray()->all();
//$items = IdStatus::find()->all()->toArray('kod_status');
//\Yii::$app->response->format = 'json';
//var_dump($items);
foreach ($items as $value) {
    $list = '<b>'.$value['kod_status']. '</b> - '. $value['status'].'<br>';
}
//VarDumper::dump($items->status);
//$items->getActiveValidators();
/* @var $this yii\web\View */
/* @var $model common\models\MaklumatKenderaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-kenderaan-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'jenis_kenderaan')->dropDownList(ArrayHelper::map(JenisKenderaan::find()->asArray()->all(), 'jenis_kenderaan', ['jenis_kenderaan']));?>
    <?= $form->field($model, 'no_kenderaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kod_status')->dropDownList(ArrayHelper::map(IdStatus::find()->asArray()->all(), 'kod_status', ['kod_status'],['status']));?>
    <?php // echo  $form->field($model, 'kod_status')->dropDownList($items);?>
    <?php //  echo 
//    $form->field($model, 'kod_status')
//        ->dropDownList(
////                $value['kod_status']=>[''.$value['kod_status']. '-'$value['status'].''],
//        ['B'=>'B - list1','A'=>'A - list1','V'=>'V - list1'],           // Flat array ('id'=>'label')
//            ['prompt'=>'Select status..']    // options
//        );
    ?>
    <?= $form->field($model, 'km')->textInput() ?>

    <?php if (Yii::$app->user->identity->role === 'Ketua Jabatan') { ?>
    <?= $form->field($model, 'penugasan')->dropDownList(ArrayHelper::map(JenisPenugasan::find()->asArray()->all(), 'jenis_penugasan', ['jenis_penugasan'])); ?>

    <?= $form->field($model, 'pegangan')->textInput(['maxlength' => true]) ?>

    <?=
            $form->field($model, 'kod_status')
            ->dropDownList(['Pending'=>'Pending','Aproved'=>'Aproved','Revoked'=>'Revoked'],           // Flat array ('id'=>'label')
            ['prompt'=>'Select status..']);
    ?>
    
    <?=
            $form->field($model, 'availability')
            ->dropDownList(['Active'=>'Active','Inactive'=>'Inactive'],           // Flat array ('id'=>'label')
            ['prompt'=>'Select status..']);
    ?>
    <?php } ?>

    <?= $form->field($model, 'catatan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
