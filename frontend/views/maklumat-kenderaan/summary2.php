<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\JenisKenderaan;
use common\models\JenisPenugasan;
use common\models\MaklumatKenderaan;
use common\models\Summary;
use yii\db\Query;
use kartik\grid\GridView;

/*  @var $this yii\web\View  */
/*  @var $searchModel common\models\MaklumatKenderaanSearch  */
/*  @var $dataProvider yii\data\ActiveDataProvider  */

$this->title = Yii::t('app', 'Maklumat Kenderaan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-kenderaan-index">

        <h1><?php echo Html::encode($this->title) ?></h1>

        

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute'=>'pegangan', 
            'width'=>'310px',
            'value'=>function ($model, $key, $index, $widget) { 
                return $model->pegangan;
            },
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>ArrayHelper::map(MaklumatKenderaan::find()->orderBy('pegangan')->asArray()->all(), 'pegangan', 'pegangan'), 
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'Any supplier'],
            'group'=>true,  // enable grouping,
            'groupedRow'=>true,                    // move grouped column to a single grouped row
            'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
            'groupEvenCssClass'=>'kv-grouped-row', // configure even group cell css class
            'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                return [
                    'mergeColumns'=>[[0,2]], // columns to merge in summary
                    'content'=>[             // content to show in each summary cell
                        0=>'Summary (' . $model->pegangan . ')',
                        4=>GridView::F_AVG,
                        5=>GridView::F_SUM,
                        6=>GridView::F_SUM,
                    ],
                    'contentFormats'=>[      // content reformatting for each summary cell
                        4=>['format'=>'number', 'decimals'=>2],
                        5=>['format'=>'number', 'decimals'=>0],
                        6=>['format'=>'number', 'decimals'=>2],
                    ],
                    'contentOptions'=>[      // content html attributes for each summary cell
                        0=>['style'=>'font-variant:small-caps'],
                        4=>['style'=>'text-align:right'],
                        5=>['style'=>'text-align:right'],
                        6=>['style'=>'text-align:right'],
                    ],
                    // html attributes for group summary row
                    'options'=>['class'=>'danger','style'=>'font-weight:bold;']
                ];
            }
        ],
        [
            'attribute'=>'jenis_kenderaan', 
            'width'=>'250px',
            'value'=>function ($model, $key, $index, $widget) { 
                return $model->jenis_kenderaan;
            },
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>ArrayHelper::map(MaklumatKenderaan::find()->orderBy('jenis_kenderaan')->asArray()->all(), 'jenis_kenderaan', 'jenis_kenderaan'), 
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'Any jenis_kenderaan'],
            'group'=>true,  // enable grouping
            'subGroupOf'=>1, // supplier column index is the parent group,
            'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                return [
                    'mergeColumns'=>[[2, 3]], // columns to merge in summary
                    'content'=>[              // content to show in each summary cell
                        2=>'Summary (' . $model->jenis_kenderaan . ')',
                        4=>GridView::F_AVG,
                        5=>GridView::F_SUM,
                        6=>GridView::F_SUM,
                    ],
                    'contentFormats'=>[      // content reformatting for each summary cell
                        4=>['format'=>'number', 'decimals'=>2],
                        5=>['format'=>'number', 'decimals'=>0],
                        6=>['format'=>'number', 'decimals'=>2],
                    ],
                    'contentOptions'=>[      // content html attributes for each summary cell
                        4=>['style'=>'text-align:right'],
                        5=>['style'=>'text-align:right'],
                        6=>['style'=>'text-align:right'],
                    ],
                    // html attributes for group summary row
                    'options'=>['class'=>'success','style'=>'font-weight:bold;']
                ];
            },
        ],
        [
            'attribute'=>'availability',
            'pageSummary'=>'Page Summary',
            'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],
//        [
//            'attribute'=>'unit_price',
//            'width'=>'150px',
//            'hAlign'=>'right',
//            'format'=>['decimal', 2],
//            'pageSummary'=>true,
//            'pageSummaryFunc'=>GridView::F_AVG
//        ],
//        [
//            'attribute'=>'units_in_stock',
//            'width'=>'150px',
//            'hAlign'=>'right',
//            'format'=>['decimal', 0],
//            'pageSummary'=>true
//        ],
//        [
//            'class'=>'kartik\grid\FormulaColumn',
//            'header'=>'Amount In Stock',
//            'value'=>function ($model, $key, $index, $widget) { 
//                $p = compact('model', 'key', 'index');
//                return $widget->col(4, $p) * $widget->col(5, $p);
//            },
//            'mergeHeader'=>true,
//            'width'=>'150px',
//            'hAlign'=>'right',
//            'format'=>['decimal', 2],
//            'pageSummary'=>true
//        ],
//            'pegangan',
//            'jenis_kenderaan',
//            'kod_status',
//            'timestamp',
//            'perjawatan',
//            'penugasan',
//            'pegangan',
//            'kenderaan_rosak',
//            'status',
//            'availability',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
