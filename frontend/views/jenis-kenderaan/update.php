<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JenisKenderaan */

$this->title = 'Update Jenis Kenderaan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-kenderaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
