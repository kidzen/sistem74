<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JenisKenderaan */

$this->title = 'Create Jenis Kenderaan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-kenderaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
