<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JenisPeganganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Pegangans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-pegangan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jenis Pegangan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'jenis_pegangan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
