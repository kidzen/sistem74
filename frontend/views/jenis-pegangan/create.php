<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JenisPegangan */

$this->title = 'Create Jenis Pegangan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pegangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-pegangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
