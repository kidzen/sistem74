<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IdStatus */

$this->title = 'Create Id Status';
$this->params['breadcrumbs'][] = ['label' => 'Id Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="id-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
