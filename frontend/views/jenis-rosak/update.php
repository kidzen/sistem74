<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JenisRosak */

$this->title = 'Update Jenis Rosak: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Rosaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-rosak-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
