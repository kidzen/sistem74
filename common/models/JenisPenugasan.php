<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_penugasan".
 *
 * @property integer $id
 * @property string $jenis_penugasan
 */
class JenisPenugasan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_penugasan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_penugasan'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_penugasan' => 'Jenis Penugasan',
        ];
    }
}
