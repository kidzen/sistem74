<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "maklumat_kenderaan".
 *
 * @property integer $id
 * @property string $jenis_kenderaan
 * @property string $no_kenderaan
 * @property string $kod_status
 * @property integer $km
 * @property string $timestamp
 * @property string $penugasan
 * @property string $pegangan
 * @property string $status
 * @property string $availability
 * @property string $catatan
 */
class MaklumatKenderaan extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'maklumat_kenderaan';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['km', 'availability'], 'integer'],
            [['timestamp'], 'safe'],
            [['jenis_kenderaan', 'no_kenderaan', 'kod_status', 'perjawatan', 'penugasan', 'pegangan', 'kenderaan_rosak', 'catatan'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'jenis_kenderaan' => 'Jenis Kenderaan',
            'no_kenderaan' => 'No Kenderaan',
            'kod_status' => 'Kod Status',
            'km' => 'Km',
            'timestamp' => 'Timestamp',
            'penugasan' => 'Penugasan',
            'pegangan' => 'Pegangan',
            'status' => 'Status',
            'availability' => 'Availability',
            'catatan' => 'Catatan',
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find() {
        return new UserQuery(get_called_class());
    }

    public function getKodStatus() {
        return $this->hasOne(IdStatus::className(), ['id' => 'kod_status']);
    }

    public function getJenisKenderaan() {
        return $this->hasOne(JenisKenderaan::className(), ['id' => 'jenis_kenderaan']);
    }

}
