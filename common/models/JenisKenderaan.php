<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_kenderaan".
 *
 * @property integer $id
 * @property string $jenis_kenderaan
 */
class JenisKenderaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_kenderaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['jenis_kenderaan'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_kenderaan' => 'Jenis Kenderaan',
        ];
    }
}
