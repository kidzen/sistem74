<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MaklumatKenderaan]].
 *
 * @see MaklumatKenderaan
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MaklumatKenderaan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MaklumatKenderaan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}