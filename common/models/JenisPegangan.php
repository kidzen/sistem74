<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_pegangan".
 *
 * @property integer $id
 * @property string $jenis_pegangan
 */
class JenisPegangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_pegangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pegangan'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_pegangan' => 'Jenis Pegangan',
        ];
    }
}
