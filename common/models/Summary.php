<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "summary".
 *
 * @property integer $id
 * @property string $jenis_kenderaan
 * @property string $perkara
 * @property string $catatan
 */
class Summary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'summary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['jenis_kenderaan', 'perkara', 'catatan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_kenderaan' => 'Jenis Kenderaan',
            'perkara' => 'Perkara',
            'catatan' => 'Catatan',
        ];
    }
}
