<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[IdStatus]].
 *
 * @see IdStatus
 */
class IdStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return IdStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IdStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}