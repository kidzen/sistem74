<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_rosak".
 *
 * @property integer $id
 * @property string $jenis_rosak
 */
class JenisRosak extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_rosak';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_rosak'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_rosak' => 'Jenis Rosak',
        ];
    }
}
