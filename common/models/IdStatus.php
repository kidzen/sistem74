<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "id_status".
 *
 * @property integer $id
 * @property string $kod_status
 * @property string $status
 */
class IdStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'id_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_status', 'status'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kod_status' => 'Kod Status',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return IdStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdStatusQuery(get_called_class());
    }
    
//    public function getKodStatus() {
//        return $this->hasMany(IdStatus::className(), ['kod_status' => 'kod_status']);
//    }
}
