<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MaklumatKenderaan;

/**
 * MaklumatKenderaanSearch represents the model behind the search form about `common\models\MaklumatKenderaan`.
 */
class MaklumatKenderaanSearch extends MaklumatKenderaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'km'], 'integer'],
            [['jenis_kenderaan', 'no_kenderaan', 'kod_status', 'timestamp', 'perjawatan', 'penugasan', 'pegangan', 'kenderaan_rosak', 'status', 'availability', 'catatan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaklumatKenderaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
//            'km' => $this->km,
//            'timestamp' => $this->timestamp,
//            'timestamp' => $this->timestamp,
//            'jenis_kenderaan' => $this->jenis_kenderaan
        ]);

        $query->andFilterWhere(['like', 'jenis_kenderaan', $this->jenis_kenderaan])
            ->andFilterWhere(['like', 'no_kenderaan', $this->no_kenderaan])
            ->andFilterWhere(['like', 'kod_status', $this->kod_status])
            ->andFilterWhere(['like', 'perjawatan', $this->perjawatan])
            ->andFilterWhere(['like', 'penugasan', $this->penugasan])
            ->andFilterWhere(['like', 'pegangan', $this->pegangan])
            ->andFilterWhere(['like', 'kenderaan_rosak', $this->kenderaan_rosak])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'km', $this->km])
            ->andFilterWhere(['like', 'availability', $this->availability])
            ->andFilterWhere(['like', 'timestamp', $this->timestamp])
            ->andFilterWhere(['like', 'catatan', $this->catatan]);

        return $dataProvider;
    }
}
